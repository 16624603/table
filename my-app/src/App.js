import { Routes, Route } from "react-router-dom"
import LandingPage from "./pages/LandingPage"
import CreateAccount from "./pages/CreateAccount"
import CreateTable from "./pages/CreateTable"
import Dashboard from "./pages/Dashboard"
import DisplayTable from "./pages/DisplayTable"

function App() {
  return (
  <Routes>
    <Route path="/" element={<LandingPage />} />
    <Route path="/dashboard" element={<Dashboard />} />
    <Route path="/create-account" element={<CreateAccount />} />
    <Route path="/create-table" element={<CreateTable />} /> 
    <Route path="/display-table/:tableID" element={<DisplayTable />} /> 
  </Routes>
  )
}

export default App;
