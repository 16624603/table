import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { useNavigate, useParams  } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Form from './components/DisplayTableForm';
import { useRef } from 'react';

export default function DisplayTable() {
    const { tableID } = useParams()
    const [rows, setRows] = useState([]);
    const [columns, setColumns] = useState([]);
    const [variable1, setVariable1] = useState(1);
    const [receivedData, setReceivedData] = useState(1);
    const isInitialRender = useRef(true);
    const [columnsForForm, setColumnsForForm] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate();

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await fetch(`/display-table/view-table`, {
                method: 'POST',
                headers: { 'content-type': 'application/JSON'},
                body: JSON.stringify({ tableID }),
                credentials: 'include'
            });

            if (!response.ok) {
                return response.json().then((errorData) => {
                  console.error(`Server error: ${errorData.message}`);
                  setErrorMessage(`Server error: ${errorData.message}`);
                });
            } else {
                const result = await response.json();
                setRows(result.tableData)
                console.log(result.tableData)
                console.log('this is schema', result.schemaResult)
                const columnsResponse = result.schemaResult.map((column) => ({
                    field: column.name,
                    headerName: column.name,
                    width: 100,
                  }));
                setColumns(columnsResponse)
                setColumnsForForm(result.schemaResult)
                console.log('this is columns', columns)
            }
            } catch (error) {
                console.error('Error fetching data:', error.message);
                setErrorMessage(`Server error: ${error.message}`);
            }
        };
    
        fetchData();
      }, []);

      useEffect(() => {
        if (!isInitialRender.current) {
          const fetchData = async () => {
            try {
              const response = await fetch(`/display-table/update-view-table`, {
                method: 'POST',
                headers: { 'content-type': 'application/JSON' },
                body: JSON.stringify({ tableID }),
                credentials: 'include',
              });
    
              if (!response.ok) {
                  return response.json().then((errorData) => {
                      console.error(`Server error: ${errorData.message}`);
                      setErrorMessage(`Server error: ${errorData.message}`);
                  });
              } else {
                const result = await response.json();
                setRows(result.tableData);
                console.log(result.tableData);
                console.log('this is schema', result.schemaResult);
    
                const columnsResponse = result.schemaResult.map((column) => ({
                  field: column.name,
                  headerName: column.name,
                  width: 100,
                }));
    
                setColumns(columnsResponse);
                setColumnsForForm(result.schemaResult);
                console.log('this is columns', columns);
              }
            } catch (error) {
              console.error('Error fetching data:', error.message);
              setErrorMessage(`Server error: ${error.message}`);
            }
          };
    
          fetchData();
        } else {
          isInitialRender.current = false;
        }
      }, [variable1]);

      const handleClick = () => {
        navigate('/dashboard');
      };

      const getRowId = (row) => row.id || Math.random();

      return (
        <>
        <Box
            sx={{
            height: 300,
            width: 500,
            margin: '10px auto', // Top margin of 50px, horizontally centered
            }}
        >
        <DataGrid
            rows={rows}
            getRowId={getRowId}
            columns={columns} 
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            checkboxSelection
          />
        </Box>
        <div className="form-container">
        <Form
        variable1={variable1}
        setVariable1={setVariable1}
        tableID={tableID}
        columnsForForm={columnsForForm}
        />
        </div>
        <p>Error messages:</p>
        <pre>{JSON.stringify(errorMessage)}</pre>
        <button className="dashboard-button" type="button" onClick={handleClick}>Dashboard</button>
        </>
      );
}
