import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react';

export default function Dashboard() {

    const [rows, setRows] = useState([]);
    const [email, setEmail] = useState([]);
    const navigate = useNavigate();
    const [errorMessage, setErrorMessage] = useState('');

    const columns = [
        { field: 'tableID', headerName: 'Table ID', width: 200 },
        { field: 'tableName', headerName: 'Table Name', width: 200 },
      ];

    useEffect(() => {
        const fetchData = async () => {
          try {
              const response = await fetch('/display-table/display-list-of-tables', {
              method: 'POST',
              credentials: 'include'
              });
              if (!response.ok) {
                  return response.json().then((errorData) => {
                    if (errorData.message === 'Failed to confirm session') {
                        console.error(`Server error: ${errorData.message}`);
                        setErrorMessage(`Server error: ${errorData.message}`); 
                    } else if (response.ok) {
                        navigate('/')
                    }
                  });
              } else {
                  const result = await response.json();
                  setRows(result.tableList);
                  setEmail(result.email);
                  console.log('rows:', JSON.stringify(rows));
                  console.log('email:', JSON.stringify(email));
              }
          } 
          catch (error) {
              console.error('Error fetching data:', error.message);
              navigate('/');
          }
        };
        
        fetchData();
      }, []);

      const handleRowClick = (params) => {
        console.log('params.row.tableID', params.row.tableID);
        navigate(`/display-table/${params.row.tableID}`);
      };

      const handleClick = () => {
        navigate('/create-table');
      };

    return (
      <>
        <Box
            sx={{
            height: 300,
            width: 500,
            margin: '50px auto 0', // Top margin of 50px, horizontally centered
            }}
        >
        <DataGrid
            rows={rows}
            columns={columns}
            getRowId={(row) => row.tableID}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            checkboxSelection
            disableRowSelectionOnClick
            onRowClick={handleRowClick}
          />
        </Box>
        <button type="button" onClick={handleClick}>Create Table</button>
        <pre style={{ fontSize: '20px' }}>{`Server message: Logged in ${email}`}</pre>
        <p>Error messages:</p>
        <pre>{JSON.stringify(errorMessage)}</pre>
        </>
      );
    
}
