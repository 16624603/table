import { useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react';

export default function CreateTable() {

    const [arrayInput, setArrayInput] = useState([['', 'INTEGER', 'NULL', 'NOTUNIQUE'], 
                                                  ['', 'INTEGER', 'NULL', 'NOTUNIQUE'], 
                                                  ['', 'INTEGER', 'NULL', 'NOTUNIQUE']])
    const [tableName, setTableName] = useState('')
    const [errorMessage, setErrorMessage] = useState('');

    const navigate = useNavigate()


    const createTable = async (arrayInput, tableName, e) => {    
        e.preventDefault();

        if (!arrayInput) {
            setErrorMessage('Invalid arrayInput');
            return
        }

        fetch('/table-creator/create-table', {
            method: 'POST',
            headers: { 'content-type': 'application/JSON'},
            body: JSON.stringify({ arrayInput, tableName }),
            credentials: 'include'
        })
        .then(response => {
        if (!response.ok) { 
            return response.json().then((errorData) => {
                console.error(`Server error: ${errorData.message}`);
                setErrorMessage(`Server error: ${errorData.message}`);
            });
        } else if (response.ok) {
            navigate('/dashboard')
        }
        })
        .catch(error => {
        if (error.message === 'User is not logged in/ Failed to access database') {
            console.error(`Server error: ${error.message}`);
            navigate('/')
        } else {
            setErrorMessage(`Server error: ${error.message}`);
        }
        })
    }

    const change = (e, rowIndex, columnIndex) => {
        e.preventDefault();
        const newArrayInput = arrayInput.map((oldRow, oldRowIndex) => (
            oldRowIndex === rowIndex ? [...oldRow.slice(0, columnIndex), e.target.value, ...oldRow.slice(columnIndex + 1)] : oldRow
        ));
        setArrayInput(newArrayInput);
    };

    return (
        <div>
            <p>create table</p>
            <form onSubmit={(e)=>createTable(arrayInput, tableName, e)}>
            {arrayInput.map((row, rowIndex) => (
            <div key={rowIndex}>
             <input type="text" onChange={(e) => change(e, rowIndex, 0)}/>
             <select  onChange={(e) => change(e, rowIndex, 1)}>
                <option value="INTEGER">INTEGER</option>
                <option value="TEXT">TEXT</option>
                <option value="BOOLEAN">BOOLEAN</option>
                <option value="DATE">DATE</option>
            </select>
            <select onChange={(e) => change(e, rowIndex, 2)}>
                <option value="NULL"></option>
                <option value="NOTNULL">NON NULL</option>
            </select>
            <select onChange={(e) => change(e, rowIndex, 3)}>
                <option value="NOTUNIQUE"></option>
                <option value="UNIQUE">UNIQUE</option>
            </select>
            
            </div>
            ))}
            <p>insert table name</p>
            <input type="text" onChange={(e) => setTableName(e.target.value)}/>
            <button type="submit">Submit</button>
            </form>

            <button type="button" onClick={() => setArrayInput([...arrayInput, ['', 'INTEGER', 'NULL', 'NOTUNIQUE']])}>Add row</button>
            <button type="button" onClick={() => setArrayInput(arrayInput.slice(0, -1)) }>Remove row</button>

            <div>
            <p>The current value of arrayInput is:</p>
            <pre>{JSON.stringify(arrayInput)}</pre>
            <pre>{JSON.stringify(tableName)}</pre>
            <p>Error messages:</p>
            <pre>{JSON.stringify(errorMessage)}</pre>
            </div>
            <p>Error messages:</p>
            <pre>{JSON.stringify(errorMessage)}</pre>
        </div>
        )
}

