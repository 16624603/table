import { useNavigate } from 'react-router-dom'
import { useState } from 'react';
import './global.css';

export default function LandingPage() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate()

    const enterAccount = async (email, password, e) => {    
        e.preventDefault();

        if (!email || !password) {
            setErrorMessage('Invalid arrayInput');
            return
        }
    
        fetch('/account/enter-account', {
            method: 'POST',
            headers: { 'content-type': 'application/JSON'},
            body: JSON.stringify({email, password}),
            credentials: 'include'

        })
        .then(response => {
            if (!response.ok) { 
                return response.json().then((errorData) => {
                    console.error(`Server error: ${errorData.message}`);
                    setErrorMessage(`Server error: ${errorData.message}`);
                });
            } else if (response.ok) {
                navigate('/dashboard')
            }

        })
        .catch(error => {
            console.error('Server error:', error.message);
            setErrorMessage(`Server error: ${error.message}`);
        })
    }

    return (
        <div>
            <div>
                <p >Enter</p>
                <form onSubmit={(e) => enterAccount(email, password, e)} >
                    <label >Email <input value={email} type="email" onChange={(e) => setEmail(e.target.value)}  /></label>
                    <label >Password <input value={password} type="password" onChange={(e) => setPassword(e.target.value)}/></label>
                    <button type="submit" >Submit</button>
                </form>
                <div>
                    <p>Error messages:</p>
                    <pre>{JSON.stringify(errorMessage)}</pre>
                </div>
                <button type="button" onClick={() => navigate('/create-account')} >Create Account</button>
            </div>
        </div>
    );
}