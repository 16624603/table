import React, { useState } from 'react';

// to do: change name of setvariable and setreceiveddata
function Form({ setVariable1, setReceivedData, tableID, columnsForForm }) {

    const [errorMessage, setErrorMessage] = useState('');

    const initialFormState = Object.fromEntries(columnsForForm.map((field) => [
      field.name,
      field.type === 'INTEGER' ? null : ''
    ]));
    console.log('INITIALFORMSTATE:', JSON.stringify(initialFormState))
    const [form, setForm] = useState(initialFormState);
  
    const handleChange = (fieldName, value) => {
      setForm((prevForm) => ({ ...prevForm, [fieldName]: value }));
    };
  
    const handleSubmit = (address, e) => {
        e.preventDefault();
        console.log('Form:', JSON.stringify(form))
        fetch(address, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json'},
          body: JSON.stringify({ form, tableID, columnsForForm }),
          credentials: 'include',
        })
        .then((response) => {
            if (!response.ok) {
              return response.json().then((errorData) => {
                console.error(`Server error: ${errorData.message}`);
                setErrorMessage(`Server error: ${errorData.message}`);
              });
            } else if (response.ok) {
                setVariable1((prev) => prev + 1);
            }
          })
        .catch((error) => {
            console.error('Error submitting form:', error.message);
            setErrorMessage(`Server error: ${error.message}`);
        });
    };
  
    return (
        <form>
          {columnsForForm.map((field) => (
            <div key={field.name}>
              <label htmlFor={field.name}>{field.name}</label>
              <input
                type="text"
                id={field.name}
                name={field.name}
                value={form[field.name]}
                onChange={(e) => handleChange(field.name, e.target.value)}
              />
            </div>
          ))}
          <button type="button" onClick={(e) => handleSubmit('/table/add-data', e)}>Submit</button>
          <button type="button" onClick={(e) => handleSubmit('/table/update-data', e)}>Update</button>
          <button type="button" onClick={(e) => handleSubmit('/table/delete-data', e)}>Delete</button>
          <button type="button" onClick={(e) => handleSubmit('/table/save-data', e)}>Save</button>
          {/* <button type="button" onClick={(e) => handleSubmit('/table/revert-to-last-save', e)}>Undo</button> */}
          <p>Error messages:</p>
          <pre>{JSON.stringify(errorMessage)}</pre>
        </form>
        
    );

}
export default Form;


