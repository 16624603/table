const express = require('express');
const app = express.Router();
app.use(express.json())
const db = require('./databaseConnection');
const setupSession = require('./setupSession');

app.use(setupSession());

db.run(`
    CREATE TABLE IF NOT EXISTS account (
    accountID INTEGER PRIMARY KEY AUTOINCREMENT,
    email TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL
)`);  

app.post('/create-account', async (req, res) => {

    try {

        const { email, password } = req.body
        console.log(email, password)
        db.run('INSERT INTO account (email, password) VALUES (?, ?)', [email, password], (err) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ message: 'Failed to create account' });
        }
        res.status(201).json({ message: 'Account created successfully' });
        });

    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: error.message });
    }
    
});

app.post('/enter-account', async (req, res) => {

    try {

        const { email, password } = req.body;
        console.log(email, password);

        // Check if the user exists in the database
        const query = 'SELECT * FROM account WHERE email = ?';
        db.get(query, [email], (err, stored) => {
            if (err) {
                return res.status(500).json({ message: 'Could not access database' });
            }

            if (!stored) {
                // User not found
                return res.status(404).json({ message: 'User not found' });
            }

            // Check if the input password matches the password stored in the database
            if (!(password === stored.password)) {
                return res.status(401).json({ message: 'Password is incorrect' });
            }

            req.session.user = {
                email: email,
                password: password
            };

            let user = req.session.user;
            res.cookie('user', stored);
            return res.status(200).json({ message: 'Authentication successful' });
        });
        
    } catch (error) {
        console.error('Error:', error);
        return res.status(500).json({ message: error.message });
    }
});

module.exports = app