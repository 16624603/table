const express = require('express');
const app = express.Router();
app.use(express.json())
const db = require('./databaseConnection');
const setupSession = require('./setupSession');

app.use(setupSession());

const confirmSession = (email, password) => {
    db.serialize(() => {
    db.get('SELECT * FROM account WHERE email = ?', [email], (err, storedAccountDetails) => {
    console.log(storedAccountDetails);
  
    if (err) {
        console.error(err);
        return res.status(500).json({ message: 'Failed to confirm session' });
    }
  
    if (!storedAccountDetails) {
        return res.status(500).json({ message: 'Failed to confirm session' });
    }
  
    const storedPassword = storedAccountDetails.password;
  
    if (password !== storedPassword) {
        return res.status(500).json({ message: 'Failed to confirm session' });
    }
  
    console.log({ message: 'Authentication successful' });
    });
    })
};

module.exports = confirmSession;