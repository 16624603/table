const express = require('express');
const app = express.Router();
app.use(express.json())
const db = require('./databaseConnection');
const setupSession = require('./setupSession');
const confirmSession = require('./confirmSession');

app.use(setupSession());

    app.post('/add-data', async (req, res) => {
      try {
          const email = req.cookies.user.email;
          const password = req.cookies.user.password;
          console.log('Received session:', req.cookies.user);
          console.log(email);
          console.log(password);
          confirmSession(email, password);

          console.log('Received request body:', req.body);
          const { form, tableID } = req.body;
          console.log(req.body);
        
          let columns;
          let values;
          let insertQuery;

          if (form.id === '' || form.id === null || form.id === undefined || typeof form.id === 'string') {
              delete form.id;
              columns = Object.keys(form);
              values = Object.values(form);
          } else {
              columns = Object.keys(form);
              values = Object.values(form);
          }  

          insertQuery = `INSERT INTO temporary${tableID} (${columns.join(', ')}) VALUES (${columns.map(() => '?').join(', ')})`;

          // Construct dynamic INSERT query
          

          db.run(insertQuery, values, function (err) {
              try {
                  if (err) {
                      console.error('Error inserting data:', err);
                      let errorMessage;
                      if (err.message.includes('constraint violation')) {
                          errorMessage = 'Data insertion failed: Duplicate entry or constraint violation';
                      } else {
                          errorMessage = 'Internal Server Error';
                      }
                      return res.status(500).json({ message: errorMessage });
                  } else {
                      console.log(`Data inserted into ${tableID} with ID`);
                      res.status(200).json({ message: 'Data inserted successfully' });
              }
              } catch (error) {
                  console.error('Error:', error);
                  res.status(500).json({ message: error.message });
              }
          });
      } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: error.message });
      }
    });

    app.post('/update-data', async (req, res) => {
      try {
        const email = req.cookies.user.email;
        const password = req.cookies.user.password;
        console.log('Received session:', req.cookies.user);
        console.log(email);
        console.log(password);
        confirmSession(email, password);

        console.log('Received request body:', req.body);
        const { form, tableID } = req.body;
        console.log(req.body);

        const id = form.id;
        const columns = Object.keys(form);
        const values = Object.values(form);

        const updateQuery = `UPDATE temporary${tableID} SET ${columns.map(col => `${col} = ?`).join(', ')} WHERE id = ?`;
        values.push(id);
        
        db.run(updateQuery, values, function (err) {
          if (err) {
            console.error('Error inserting data:', err);

            let errorMessage;
            if (err.message.includes('constraint violation')) {
              errorMessage = 'Data insertion failed: Duplicate entry or constraint violation';
            } else {
              errorMessage = 'Internal Server Error';
            }

            return res.status(500).json({ message: errorMessage });
          } else {
            console.log(`Data inserted into ${tableID}`);
            res.status(200).json({ message: 'Data inserted successfully' });
          }
        });
      } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: error.message });
      }
    });

    app.post('/delete-data', async (req, res) => {
      try {
        const email = req.cookies.user.email;
        const password = req.cookies.user.password;
        console.log('Received session:', req.cookies.user);
        console.log(email);
        console.log(password);
        confirmSession(email, password);

        console.log('Received request body:', req.body);
        const { form, tableID } = req.body;
        console.log(req.body);

        const id = form.id;
        
        const deleteQuery = `DELETE FROM temporary${tableID} WHERE id = ?`;
        
        db.run(deleteQuery, [id], function (err) {
            if (err) {
                console.error('Error inserting data:', err);
                let errorMessage;
                if (err.message.includes('constraint violation')) {
                errorMessage = 'Data insertion failed: Duplicate entry or constraint violation';
                } else {
                errorMessage = 'Internal Server Error';
                }
                return res.status(500).json({ message: errorMessage });
            } else {
                console.log(`Data inserted into ${tableID}`);
                res.status(200).json({ message: 'Data inserted successfully' });
            }
        })
      } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: error.message });
      }
    });

    app.post('/save-data', async (req, res) => {
      try {
      const email = req.cookies.user.email;
      const password = req.cookies.user.password;
      console.log('Received session:', req.cookies.user);
      console.log(email);
      console.log(password);
      confirmSession(email, password);
    
      console.log('Received request body:', req.body);
      const { tableID } = req.body;
      console.log(req.body);
    
      // Step 1: Erase all data from history${tableID}
      const eraseHistoryQuery = `DELETE FROM history${tableID}`;
      db.run(eraseHistoryQuery, function (err) {
        if (err) {
          console.error('Error erasing data from history:', err);
          return res.status(500).json({ message: 'Error while trying to save table' });
        }

        // Step 2: Copy all data from ${tableID} into history${tableID}
        const copyToHistoryQuery = `INSERT INTO history${tableID} SELECT * FROM ${tableID}`;
        db.run(copyToHistoryQuery, function (err) {
          if (err) {
            console.error('Error copying data to history:', err);
            return res.status(500).json({ message: 'Error while trying to save table' });
          }

          // Step 3: Delete all data from ${tableID}
          const deleteFromTableQuery = `DELETE FROM ${tableID}`;
          db.run(deleteFromTableQuery, function (err) {
            if (err) {
              console.error('Error deleting data from table:', err);
              return res.status(500).json({ message: 'Error while trying to save table' });
            }

            // Step 4: Copy all data from temporary${tableID} into ${tableID}
            const copyFromTemporaryQuery = `INSERT INTO ${tableID} SELECT * FROM temporary${tableID}`;
            db.run(copyFromTemporaryQuery, function (err) {
              if (err) {
                console.error('Error copying data from temporary:', err);
                return res.status(500).json({ message: 'Error while trying to save table' });
              }

              console.log(`Data saved successfully for ${tableID}`);
              res.status(200).json({ message: 'Data saved successfully' });
            });
          });
        });
      });
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ message: error.message });
    }
    });
    // this feature is not working
    app.post('/revert-to-last-save', async (req, res) => { 
      console.log('Revert to last save');
      try {
      const email = req.cookies.user.email;
      const password = req.cookies.user.password;
      confirmSession(email, password);
    
      const { tableID } = req.body;
    
      // Step 1: Erase all data from ${tableID}
      const eraseHistoryQuery = `DELETE FROM history${tableID}`;
      db.run(eraseHistoryQuery, function (err) {
        if (err) {
          console.error('Error erasing data from history:', err);
          return res.status(500).json({ message: 'Error while trying to save table' });
        }

        // Step 2: Copy all data from history${tableID} into ${tableID}
        const copyToHistoryQuery = `INSERT INTO history${tableID} SELECT * FROM ${tableID}`;
        db.run(copyToHistoryQuery, function (err) {
          if (err) {
            console.error('Error copying data to history:', err);
            return res.status(500).json({ message: 'Error while trying to save table' });
          }
          // Step 3: Erase all data from temporary${tableID}
          const eraseTemporaryQuery = `DELETE FROM temporary${tableID}`;
          db.run(eraseTemporaryQuery, function (err) {
            if (err) {
              console.error('Error erasing data from temporary:', err);
              return res.status(500).json({ message: 'Error while trying to save table' });
            }
            // Step 4: Copy all data from history${tableID} into temporary${tableID}
            const copyToHistoryQuery = `INSERT INTO temporary${tableID} SELECT * FROM history${tableID}`;
            db.run(copyToHistoryQuery, function (err) {
              if (err) {
                console.error('Error copying data to history:', err);
                return res.status(500).json({ message: 'Error while trying to save table' });
              }
            })
          })
        });
      });
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ message: error.message });
    }
    });

module.exports = app;







