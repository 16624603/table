const express = require('express');
const app = express.Router();
app.use(express.json());
const db = require('./databaseConnection');
const setupSession = require('./setupSession');
const confirmSession = require('./confirmSession');

app.use(setupSession());

    app.post('/display-list-of-tables', async (req, res) => {
        try {
            const email = req.cookies.user.email;
            const password = req.cookies.user.password;
            console.log('Received session:', req.cookies.user);
            console.log(email);
            console.log(password);
            confirmSession(email, password);

            db.get('SELECT accountID FROM account WHERE email = ?', [email], (err, accountID) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ message: 'Failed to display list of tables' });
                }
                console.log('AccountID is:', accountID);
                console.log('AccountID property is:', accountID.accountID);
                db.all('SELECT tableID, tableName FROM tableList WHERE accountID = ?', [accountID.accountID], (err, tableList) => {
                    if (err) {
                        console.error(err);
                        return res.status(500).json({ message: 'Failed to display list of tables' });
                    }
                    console.log('The result is:', tableList);
                    res.status(200).json({ tableList, email });
                });
            });
        } catch (error) {
            console.error('Error:', error);
            res.status(500).json({ message: error.message });
        }
    });

    app.post('/view-table', async (req, res) => {
        try {
        const email = req.cookies.user.email;
        const password = req.cookies.user.password;
        const tableID = req.body.tableID;
        console.log('Received session:', req.cookies.user);
        console.log(email);
        console.log(password);
        confirmSession(email, password);

        const schemaQuery = `PRAGMA table_info(${tableID})`;
        db.all(schemaQuery, (err, schemaResult) => {
            if (err) {
                console.error(err);
                return res.status(500).json({ message: 'Failed to view table' });
            }
            const dataQuery = `DELETE FROM temporary${tableID}`;
            db.all(dataQuery, (err) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ message: 'Failed to view table' });
                }
                const copyToTemporary = `INSERT INTO temporary${tableID} SELECT * FROM ${tableID}`;
                db.all(copyToTemporary, (err) => {
                    if (err) {
                        console.error(err);
                        return res.status(500).json({ message: 'Failed to view table' });
                    }
                    const dataQuery = `SELECT * FROM temporary${tableID}`;
                    db.all(dataQuery, (err, tableData) => {
                    if (err) {
                        console.error(err);
                        return res.status(500).json({ message: 'Failed to view table' });
                    }
                    res.status(200).json({ schemaResult, tableData });
                    })
                })
            })      
        })
    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: error.message });
    }
    });

    app.post('/update-view-table', async (req, res) => {
        try {
        const email = req.cookies.user.email;
        const password = req.cookies.user.password;
        const tableID = req.body.tableID;
        console.log('Received session:', req.cookies.user);
        console.log(email);
        console.log(password);
        confirmSession(email, password);

        const schemaQuery = `PRAGMA table_info(${tableID})`;
        db.all(schemaQuery, (err, schemaResult) => {
            if (err) {
                console.error(err);
                return res.status(500).json({ message: 'Failed to update table' });
            } 
            const dataQuery = `SELECT * FROM temporary${tableID}`;
            db.all(dataQuery, (err, tableData) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ message: 'Failed to update table' });
                }
                res.status(200).json({ schemaResult, tableData });
            })
        });
    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: error.message });
    }
    });


module.exports = app;
