const express = require('express');
const app = express.Router();
app.use(express.json())
const db = require('./databaseConnection');
const setupSession = require('./setupSession');
const confirmSession = require('./confirmSession');

app.use(setupSession());

db.run(`
    CREATE TABLE IF NOT EXISTS tableList (
    tableID TEXT UNIQUE,
    tableName TEXT NOT NULL,
    accountID INTEGER,
    FOREIGN KEY(accountID) REFERENCES account(accountID) ON DELETE CASCADE
)`);

app.post('/create-table', async (req, res) => {
    
    try {
        const email = req.cookies.user.email;
        const password = req.cookies.user.password;
        console.log('Received session:', req.cookies.user);
        console.log(email);
        console.log(password);
        confirmSession(email, password)

        console.log('Received request body:', req.body);
        const { arrayInput, tableName } = req.body;

        console.log('Array Input:', arrayInput);
        console.log('Table Name:', tableName);

        // validate tablename
        if (!(typeof tableName === 'string')) {
            return res.status(500).json({ message: 'Table name is not valid' });
        }

        //validate number of rows
        if (arrayInput.length > 5) {
            return res.status(500).json({ message: 'Number of columns must 5 or less' });
        }

        //validate number of items per row
        for (let i = 0; i < arrayInput.length; i++) {
            const subArray = arrayInput[i];
            if (subArray.length !== 4) {
                return res.status(500).json({ message: `Sub-array at index ${i} must have a length of exactly 4.` });
            }
        }
        
        // validate each item in the input
        for (let i = 0; i < arrayInput.length; i++) { 
            console.log(arrayInput[i][0])
            if (typeof arrayInput[i][0] !== 'string') {
                return res.status(500).json({ message: 'Error in item 0' });
            }
            console.log(arrayInput[i][1])
            if (!(arrayInput[i][1] === 'TEXT' || arrayInput[i][1] === 'INTEGER' || arrayInput[i][1] === 'BOOLEAN' || arrayInput[i][1] === 'DATE')) {
                return res.status(500).json({ message: 'Error in item 1' });
            }
            console.log(arrayInput[i][2])
            if (!(arrayInput[i][2] === 'NULL' || arrayInput[i][2] === 'NOTNULL')) {
                return res.status(500).json({ message: 'Error in item 2' });
            }
            console.log(arrayInput[i][3])
            if (!(arrayInput[i][3] === 'UNIQUE' || arrayInput[i][3] === 'NOTUNIQUE')) {
                return res.status(500).json({ message: 'Error in item 3' });
            }
        }
        console.log('array passed validation')

        // This is a temporary code
        // Register table in the table list 
        const characters1 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        const characters2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';

        let randomString = '';
        const randomIndex = Math.floor(Math.random() * characters1.length);
        randomString += characters1.charAt(randomIndex);

        for (let i = 0; i < 10; i++) {
          const randomIndex = Math.floor(Math.random() * characters2.length);
          randomString += characters2.charAt(randomIndex);
        }
        console.log(randomString);
        db.get(`SELECT * FROM tableList WHERE tableID = ?`, [randomString], (err, result) => {  
        if (randomString === result) {
            randomString = '';
                for (let i = 0; i < 10; i++) {
                    const randomIndex = Math.floor(Math.random() * characters.length);
                    randomString += characters.charAt(randomIndex);
                }
            }
        })
        console.log(randomString);

        db.get('SELECT accountID FROM account WHERE email = ?', [email], (err, result) => {
            console.log(result)
            if (err) {
                console.error(err);
                return res.status(500).json({ message: 'Failed to access database' });
            } 
            if (!result) {
                // Handle case when no account with the given email is found
                return res.status(500).json({ message: 'Account not found' });
            }
            const accountID = result.accountID;
            db.run('INSERT INTO tableList (tableID, tableName, accountID) VALUES (?, ?, ?)', [randomString, tableName, accountID], (err) => {
                if (err) {
                    return res.status(500).json({ message: 'Failed to register table' });
                } else {
                    console.log('Registered a new table')
                }
            })
        })

        setTimeout(() => {}, 100); // It was a bad idea to nest queries instead of using db.serialize, this is to prevent duplicate header error

        // Start building the SQL command
        let main = `CREATE TABLE IF NOT EXISTS ${randomString} (\n`;
        let temporary = `CREATE TABLE IF NOT EXISTS temporary${randomString} (\n`;
        let history = `CREATE TABLE IF NOT EXISTS history${randomString} (\n`;

        let sql = `id INTEGER PRIMARY KEY AUTOINCREMENT,\n`;
        
        // Iterate through columns to construct column definitions
        for (let i = 0; i < arrayInput.length; i++) {
            const [columnName, dataType, constraints, unique] = arrayInput[i];
            let columnDefinition = `"${columnName}" ${dataType} ${constraints === ' NOTNULL' ? ' NOT NULL' : ''} ${unique === ' NOTUNIQUE' ? '' : ' UNIQUE'}`;
            sql += `${columnDefinition}${i < arrayInput.length - 1 ? ',' : ''}\n`;
        }

        // Close the CREATE TABLE statement
        sql += `)`;
        console.log(sql);  

        let resultMain = main + sql
        let resultTemporary = temporary + sql
        let resultHistory = history + sql

        // Create the table
        // Create the main table

        db.run(resultMain, (err) => {
            if (err) {
                console.error(err);
                return res.status(400).json({ message: 'Failed to create table' });
            }
            db.run(resultTemporary, (err) => {
                if (err) {
                    console.error(err);
                    return res.status(400).json({ message: 'Failed to create table' });
                }
                db.run(resultHistory, (err) => {
                    if (err) {
                        console.error(err);
                        return res.status(400).json({ message: 'Failed to create table' });
                    } else {
                        console.log('Table created successfully')
                        return res.status(201).json({ message: 'Table created successfully' });
                    }
                })   
            })  
        });

    } catch (error) {
        console.log('Error:', error.message);
        return res.status(400).json({ message: error.message });
    }
})

module.exports = app






