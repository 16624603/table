const session = require('express-session');
const crypto = require('crypto');
const express = require('express');

const setupSession = () => {
    const secretKey = crypto.randomBytes(32).toString('hex'); 

    const sessionMiddleware = session({
        secret: secretKey, // Move to .env file
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 3600000,
            secure: false,
            httpOnly: true
        }
    });

    return sessionMiddleware;
};

module.exports = setupSession;